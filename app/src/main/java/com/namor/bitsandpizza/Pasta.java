package com.namor.bitsandpizza;

public class Pasta extends AbstractFood {
    public Pasta(String name, int imageResourceId) {
        super(name, imageResourceId);
    }

    public final static Pasta[] pasta = {
            new Pasta("Spaghetti Bolognese", R.drawable.spaghetti_bolognese),
            new Pasta("Lasagna", R.drawable.lasagna)
    };
}
