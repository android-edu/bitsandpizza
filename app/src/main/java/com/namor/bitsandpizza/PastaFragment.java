package com.namor.bitsandpizza;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.LinkedList;


/**
 * A simple {@link Fragment} subclass.
 */
public class PastaFragment extends Fragment {


    public PastaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        RecyclerView recyclerView = (RecyclerView)
                inflater.inflate(R.layout.fragment_pasta, container, false);

        String[] names = new String[Pasta.pasta.length];
        int[] imageResourceIds = new int[Pasta.pasta.length];

        for (int i = 0; i < Pasta.pasta.length; i++) {
            names[i] = Pasta.pasta[i].getName();
            imageResourceIds[i] = Pasta.pasta[i].getImageResourceId();
        }

        CaptionedImagesAdapter adapter = new CaptionedImagesAdapter(names, imageResourceIds);
        recyclerView.setAdapter(adapter);

        GridLayoutManager layoutManager = new GridLayoutManager(inflater.getContext(), 2);
        recyclerView.setLayoutManager(layoutManager);

        return recyclerView;
    }

    //@Override
    //public View onCreateView(LayoutInflater inflater, ViewGroup container,
    //                         Bundle savedInstanceState) {
    //    // Inflate the layout for this fragment
    //    setListAdapter(new ArrayAdapter<String>(
    //            inflater.getContext(),
    //            android.R.layout.simple_list_item_1,
    //            getResources().getStringArray(R.array.pasta)
    //    ));

    //    return super.onCreateView(inflater, container, savedInstanceState);
    //}
}
