package com.namor.bitsandpizza;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class PizzaFragment extends Fragment {
    public PizzaFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        RecyclerView recyclerView =
                (RecyclerView) inflater.inflate(R.layout.fragment_pizza, container, false);

        String[] pizzaNames = new String[Pizza.pizzas.length];
        int[] imageResourceIds = new int[Pizza.pizzas.length];
        for (int i = 0; i < Pizza.pizzas.length; i++) {
            pizzaNames[i] = Pizza.pizzas[i].getName();
            imageResourceIds[i] = Pizza.pizzas[i].getImageResourceId();
        }

        CaptionedImagesAdapter adapter = new CaptionedImagesAdapter(pizzaNames, imageResourceIds);
        recyclerView.setAdapter(adapter);

        adapter.setListener(new CaptionedImagesAdapter.Listener() {
            @Override
            public void onClick(int position) {
                Intent intent = new Intent(getActivity(), FoodDetailActivity.class);
                intent.putExtra(FoodDetailActivity.EXTRA_PIZZA_ID, position);
                getActivity().startActivity(intent);
            }
        });

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(layoutManager);

        return recyclerView;
    }

    //@Override
    //public View onCreateView(LayoutInflater inflater, ViewGroup container,
    //                         Bundle savedInstanceState) {
    //    // Inflate the layout for this fragment
    //    ArrayAdapter<String> adapter = new ArrayAdapter<>(
    //            inflater.getContext(),
    //            android.R.layout.simple_list_item_1,
    //            getResources().getStringArray(R.array.pizzas)
    //    );
    //    setListAdapter(adapter);
    //    return super.onCreateView(inflater, container, savedInstanceState);
    //}

}
