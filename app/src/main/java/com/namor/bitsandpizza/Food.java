package com.namor.bitsandpizza;

public interface Food {
    String getName();
    int getImageResourceId();
}
