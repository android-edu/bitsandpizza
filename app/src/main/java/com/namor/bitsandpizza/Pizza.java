package com.namor.bitsandpizza;

public class Pizza extends AbstractFood {
    public Pizza(String name, int imageResourceId) {
        super(name, imageResourceId);
    }

    public static final Pizza[] pizzas = {
        new Pizza("Diavolo", R.drawable.diavolo),
        new Pizza("Funghi", R.drawable.funghi)
    };
}
