package com.namor.bitsandpizza;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

public class FoodDetailActivity extends AppCompatActivity {
    public final static String EXTRA_PIZZA_ID = "pizzaId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_detail);

        initToolbar();

        loadFoodDetails();
    }

    private void loadFoodDetails() {
        int id = getIntent().getExtras().getInt(EXTRA_PIZZA_ID);
        String name = Pizza.pizzas[id].getName();
        int imageResourceId = Pizza.pizzas[id].getImageResourceId();

        TextView textView = findViewById(R.id.food_text);
        textView.setText(name);

        ImageView imageView = findViewById(R.id.food_image);
        imageView.setImageResource(imageResourceId);
        imageView.setContentDescription(name);
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
