package com.namor.bitsandpizza;

import lombok.Data;

@Data
public abstract class AbstractFood implements Food {
    final private String name;
    final int imageResourceId;
}
